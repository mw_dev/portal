<?php

namespace App\Http\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class AuthService
{
    /**
     * @param string $secretKey
     * @return Response | bool
     */
    public function check(string $secretKey)
    {
        $response = Http::withHeaders(['Access-Token' => config('portal.access_token')])
            ->post(config('portal.api_url') . '/auth/check', ['secretKey' => $secretKey]);

        return $response->successful() ? $response->json()['data'] : false;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function loginByEmail(string $email)
    {
        $response = Http::withHeaders(['Access-Token' => config('portal.access_token')])
            ->post(config('portal.api_url') . '/auth', ['email' => $email]);

        return $response->status() === 200;
    }
}
