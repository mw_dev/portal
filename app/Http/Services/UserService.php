<?php

namespace App\Http\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UserService
{
    /**
     * @param string $email
     * @return Response | bool
     */
    public function update(string $email)
    {
        if (!$uuid = session()->get('user.id')) {

        }

        $response = Http::withHeaders([
                'Access-Token' => config('portal.access_token'),
                'uuid' => $uuid
            ]
        )->put(config('portal.api_url') . '/profile/update', ['email' => $email]);

        return $response->successful() ? $response->json()['data'] : false;
    }

}
