<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;


class HomeController extends BaseController
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('login');
    }
}
