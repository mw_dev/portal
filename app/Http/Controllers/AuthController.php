<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\CheckSecretKeyRequest;
use App\Http\Services\AuthService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;


class AuthController extends BaseController
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }


    /**
     * @param AuthLoginRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function login(AuthLoginRequest $request)
    {
        session()->flush();

        $email = $request->input('email');

        if (!$this->authService->loginByEmail($email)) {
            session()->put('error', 'User not found');
        } else {
            session()->put('success', 'Please check your email');
        }

        return redirect('/');
    }

    /**
     * @param CheckSecretKeyRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function check(CheckSecretKeyRequest $request)
    {
        $secretKey = $request->get('secretKey');

        if (!$authData = $this->authService->check($secretKey)) {
            session()->flush();
            session()->put('error', 'Token not valid. Please enter your email for resend token');
            return redirect('/');
        }
        session()->put('user', $authData['user']);

        return redirect('/dashboard');
    }


    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function logout()
    {
        session()->flush();
        return redirect('/');
    }

}
