<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateEmailRequest;
use App\Http\Services\UserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;


class DashboardController extends BaseController
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('dashboard');
    }


    /**
     * @param UpdateEmailRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function update(UpdateEmailRequest $request)
    {

        $email = $request->input('email');

        if (!$userData = $this->userService->update($email)) {
            session()->put('error', 'something wrong');
        }
        session()->put('user', $userData);

        return redirect('/dashboard');
    }
}
