<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/', [AuthController::class, 'login']);

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('session.has.user');
Route::post('/dashboard', [DashboardController::class, 'update'])->middleware('session.has.user');

Route::get('logout', [AuthController::class, 'logout']);

Route::group(
    [
        'prefix' => 'auth'
    ],
    static function () {
        Route::get('', [AuthController::class, 'check']);
    }
);




