# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/master/installation)

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    composer install
    cp .env.example .env
    php artisan key:generate

## Environment variables

- `.env` - Environment variables that should be set in this file

    ACCESS_TOKEN=generate any token -  the same token as you create in .env file in portalapi
      
    API_URL=api url
 
***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.
