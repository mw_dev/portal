<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Login</title>
</head>
<body class="d-flex flex-column">


<div class="container">
    <form method="POST" class="form-signin mb-4">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        @if (session()->has('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control mb-3" placeholder="Email address"
               required
               autofocus>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>

<link rel="stylesheet" href="{{mix('css/app.css')}}">

</body>
</html>

