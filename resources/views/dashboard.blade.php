<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Dashboard</title>
</head>

<body class="d-flex flex-column h-100 p-3">
<div class="d-flex justify-content-end w-100">
    <a class="btn btn-outline-primary" href="logout">Logout</a>
</div>

<div class="container w-100">
    <div class="py-5 text-center">
        <h2>Dashboard</h2>
    </div>

    <div class="row">
        <div class="col-md-8 order-md-1 m-0 m-auto">

            <div class="row">
                @if (session()->has('user.first_name'))
                    <div class="col-md-6 mb-3">
                        <label for="firstName">First name</label>
                        <input type="text" class="form-control" id="firstName" placeholder=""
                               value=" {{ session('user.first_name') }}" readonly>
                    </div>
                @endif
                @if (session()->has('user.last_name'))
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Last name</label>
                        <input type="text" class="form-control" id="lastName" placeholder=""
                               value=" {{ session('user.last_name') }}" readonly>
                    </div>
                @endif
            </div>
            @if (session()->has('user.email'))
                <form class="form-signin mb-4" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email"
                               value=" {{ session('user.email') }}"
                               placeholder="you@example.com">
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Change Email</button>
                </form>
            @endif
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{mix('css/app.css')}}">
</body>
</html>
