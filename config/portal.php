<?php

return [
    'api_url' => env('API_URL', 'localhost'),
    'access_token' => env('ACCESS_TOKEN', '')
];
